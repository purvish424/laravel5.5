<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {

    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('login/facebook', 'Auth\LoginController@redirectToFacebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleFacebookProviderCallback');


// dd(config('services.google'));

Route::group(['middleware' => 'auth'], function () {


	Route::get('contacts/show', 'ContactsController@show');
	Route::post('creates/contact', 'ContactsController@create');

	Route::get('contacts/edit/{id}', 'ContactsController@edit');
	Route::post('creates/update', 'ContactsController@update');
    Route::get('/contacts/userdetails', 'ContactsController@getuserdetails');
    // Route::get('/contacts/googledetails', 'ContactsController@googledetails');

    Route::get('/contacts/googledetails', function(){
    	echo "<h1>Display Value using Facades</h1>";
        echo "<hr>";
   		 dd(GL::getAllData());
   		// dd("ClientId:  ".GL::getClientId());
   		// dd("ClientId:  ".GL::getclientSecret());
	});
});