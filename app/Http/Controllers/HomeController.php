<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Address;

use App\Contact;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $user_id=Auth::id();
        $contact=Contact::where('user_id',$user_id)->first();

        
        $id = (is_null($contact))?0:$contact->id;
        return view('home',['id'=>$id]);
    }
}
