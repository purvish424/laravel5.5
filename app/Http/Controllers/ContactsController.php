<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Illuminate\Support\Facades\Auth as Auth;


class ContactsController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function show(Request $request){

        $user_id=Auth::id();
        return view('contacts.create',['user_id'=>Auth::id()]);
    }

    public function create(Request $request){


        $items = $request->validate([
            'address' => 'required|min:20',
            'user_id' => 'required',
            'mobile'=> 'required|numeric|min:10',
        ]);
        Contact::create($items);

        return redirect('/home');

    }

    public function edit(Request $request){

          $contact = Contact::find($request->id);
          return view('contacts.edit',['contact'=>$contact]);
    }

    public function update(Request $request){


          $contact = Contact::find($request->id);

          $items = $request->validate([
            'address' => 'required|min:20',
            'user_id' => 'required',
            'mobile'=> 'required|numeric|min:10',
        ]);

        
          $contact->address=$request->address;
          $contact->mobile=$request->mobile;
          $contact->save(); 
        
        return redirect('/home');
    }

    public function getuserdetails(\App\ServiceContainer\UserDetails $userDetailsData){



          // $facebook->setFacebookCred(config('services.facebook'));
          $userDetailsData->setUserDetails(Auth::id());
             // dd($facebook);
           
          echo "<h1>Display Value using Service Provider</h1>";
          echo "</br><b>Mobile :</b>".$userDetailsData->getUserMobile();
          echo "</br><b>Address :</b>".$userDetailsData->getUserAddress();

          // echo "<hr>";
          // echo "</br><b>Facebook ClientId :</b>".$facebook->getClientId();
          // echo "</br><b>Facebook SecretKey :</b>".$facebook->getclientSecret();
          // echo "</br><b>Facebook RedirectUrl :</b>".$facebook->getRedirect();

    }

    
}
