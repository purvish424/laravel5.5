<?php

namespace App\Providers;



use Illuminate\Support\ServiceProvider;
use App\SocialMedia\Facebook;
use Illuminate\Http\Request;
use App\ServiceContainer\UserDetails;
use App;

class CustomServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
   
    protected $defer=true;

    public function boot()
    {
    
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        // echo "Service Provider</br>";
        // $this->app->bind(Facebook::class, function($app)
        $this->app->bind(Facebook::class, function($app)
        {
            // echo "Service inside Provider</br>";

            return new Facebook(config('services.facebook'));
        });
        

        // $this->app->bind(UserDetails::class, function($app)
        // {
        //      return new UserDetails();
        // });
    }
}
