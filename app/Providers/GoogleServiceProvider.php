<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\SocialMedia\Google;
use Illuminate\Http\Request;

class GoogleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('gl', function($app)
        {
            // echo "Service inside Provider</br>";

            return new Google(config('services.google'));
        });
    }
}
