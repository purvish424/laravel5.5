<?php
namespace App\SocialMedia;


class Google{

	public $client_id;
	public $client_secret;
	public $redirect;

	public function __construct($google){

		// dd($google);

		$this->client_id = $google['client_id'];
		$this->client_secret = $google['client_secret'];
		$this->redirect = $google['redirect'];

	}


	public function getClientId(){
		return $this->client_id;
	}

	public function getclientSecret(){
		return $this->client_secret;
	}

	public function getRedirect(){
		return $this->redirect;
	}

	public function getAllData(){
		return [
			'client_id'=>$this->client_id,
			'client_secret'=>$this->client_secret,
			'redirect'=>$this->redirect
		];
	}
}
?>