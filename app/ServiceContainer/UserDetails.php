<?php

namespace App\ServiceContainer;
use App\Contact;

use Illuminate\Support\Facades\Auth;

class UserDetails{

    public $moble;
    public $address;
    

	  public function setUserDetails($user_id){

      $contact = Contact::where('user_id',$user_id)->first();
      $this->moble = $contact->mobile;
      $this->address = $contact->address;
    }


    public function getUserMobile(){
      return $this->moble;
    }

    public function getUserAddress(){
      return $this->address;
    }
}

?>