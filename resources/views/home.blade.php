@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif


                        You are login in.
                        </br>
                        @if($id == 0)
                        <a href="/contacts/show">Add Contact</a>
                        @else
                            <a href="/contacts/edit/{{$id}}">Edit Contact</a>
                            </br>
                                <a href="/contacts/userdetails">Get Details Using Service Provider</a>
                             </br>   
                                <a href="/contacts/googledetails">Get Details Using Facades</a>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection