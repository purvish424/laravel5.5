@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">Create Contact</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif




                                <form method="POST" action="{{ url('creates/contact') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-4 col-form-label text-md-right">{{ __('Address') }}</label>

                                        <div class="col-md-6">
                                <textarea  id="address" type="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required autofocus>
                                </textarea>
                                            @if ($errors->has('address'))
                                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-4 col-form-label text-md-right">{{ __('Mobile') }}</label>

                                        <div class="col-md-6">
                                            <input type="hidden" id="user_id" name="user_id" value="{{$user_id}}">
                                            <input  id="mobile" type="address" maxlength="10" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}" required autofocus>

                                            @if ($errors->has('mobile'))
                                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('mobile') }}</strong>
                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection